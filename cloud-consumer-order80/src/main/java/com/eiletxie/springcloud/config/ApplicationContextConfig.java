package com.eiletxie.springcloud.config;

import com.eiletxie.myrule.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.client.RestTemplate;

import javax.sound.midi.Track;

/**
 * @Author EiletXie
 * @Since 2020/3/9 14:03
 */
@Configuration
public class ApplicationContextConfig {

    @Bean
//    @Lazy
//    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return  new RestTemplate();
    }

    @Bean
    @Lazy
    public Test test() {
        return new Test();
    }
}
